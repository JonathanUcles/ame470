# AME 470, Spring 2019


## References/Links

* BASH basics:
https://www.tldp.org/LDP/Bash-Beginners-Guide/html/
(look through chapter 1-4 for sure).
* GIT basics:
https://git-scm.com/book/en/v2/Getting-Started-The-Command-Line
* AWS student credits:
https://aws.amazon.com/education/awseducate/
* HTML tutorial:
https://www.w3schools.com/html/
* CSS Tutorial:
https://www.w3schools.com/css/
* Install Node.js and mongoDB on EC2 (Ubuntu):
https://gist.github.com/tejaswigowda/f289e9bff13d152876e8d4b3281142f8
